﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
    public string Vert;
    public string Hori;
    public float maxSpeed = 5f;
    private float speed = 0.0f;
    public float acc = 10.0f;
    public float turnspeed = 30.0f;
    private float brake = 100.0f;
    private Vector2 vel;
    private Vector2 Move;
    private float dt;



	// Use this for initialization
	void Start () {

        dt = Time.deltaTime;

    }
	
	// Update is called once per frame
	void Update () {

        dt = Time.deltaTime;

        float turn = Input.GetAxis(Hori);

        if (turn > 0)
        {
            turnspeed = speed + acc * dt;

        }
        else if (turn < 0)
        {
            turnspeed = speed - acc * dt;
        }

        float forwards = Input.GetAxis(Vert);
        transform.Rotate(0, 0, -turn * turnspeed);
        if (forwards > 0)
        {
            speed = speed + acc * dt;

        }else if (forwards < 0)
        {
            speed = speed - acc * dt;
        }
        else
        {
            int speedInt = (int)System.Math.Ceiling(speed);
            if (speedInt > 0)
            {
                speed = speed - brake * dt;
            }else if(speedInt < 0){
                speed = speed + brake * dt;
            }
            else
            {
                speed = 0.0f;
            }
        }

        

        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        Vector2 dir;

        dir.x = Input.GetAxis(Hori);
        dir.y = Input.GetAxis(Vert);


        //vel = dir * maxSpeed;
        vel = Vector2.up * speed;
        Move = vel * dt;

        transform.Translate(Move, Space.Self);

        //if (Move.x > maxSpeed || Move.y > maxSpeed)
        //{
        //    Debug.Log("Max Speed Has Been Exceeded.");
        //}
        //else
        //{
        //    Debug.Log("Max Speed Not Exceeded");
        //}
	}
}
