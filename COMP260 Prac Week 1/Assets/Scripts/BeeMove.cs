﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    public float maxSpeed = 4.0F;
    public float turnSpeed = 180F;

    private Vector2 heading = Vector2.right;

    public Transform[] target;
    public Vector2 V;
    public Vector2 vel;
    private float dt;

    // gets the Closest index from the Bee to the Players
    int get_index_from_Magnitude(Transform[] arr)
    {
        int index = 0;

        for (int i = 0; i < arr.Length - 1; i++)
        {

            if (arr[i].position.magnitude < arr[i + 1].position.magnitude)
            {
                index = i;
            } else
            {
                index = i + 1;
            }
        }

        return index;
    }

	// Update is called once per frame
	void Update () {

       dt = Time.deltaTime;

        int ClosestIndex = get_index_from_Magnitude(target);

        Debug.Log("Closest Player is:" + target[ClosestIndex].name + "With a Magnitude of:" + target[ClosestIndex].position.magnitude.ToString());

        V = target[ClosestIndex].position - transform.position;

        float angle = turnSpeed * dt;

        if (V.IsOnLeft(heading))
        {
            heading = heading.Rotate(angle);
        }
        else
        {
            heading = heading.Rotate(-angle);
        }

        V = V.normalized;
        vel = V * maxSpeed;

        transform.Translate(vel * dt);
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(transform.position, V);
    }
}
